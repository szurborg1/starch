const state = {"applications":[{"entity":{"name":"a","appId":"first","labels":[],"configuration":{"httpHealthcheck":{"expectedCode":"200","urls":"http://localhost:8080/application/info"}},"id":null},"moduleStates":{"httpHealthcheck":[{"status":"GOOD","label":"a Heathcheck","properties":{"response":"{\"applicationName\":\"Starch\"}"}}]}},{"entity":{"name":"b","appId":"second","labels":[],"configuration":{"httpHealthcheck":{"urls":"http://localhost:8080/application/health","expectedCode":"200"}},"id":null},"moduleStates":{"httpHealthcheck":[{"status":"GOOD","label":"b Heathcheck","properties":{"response":"{\"status\":\"UP\"}"}}]}},{"entity":{"name":"c","appId":"third","labels":[],"configuration":{"httpHealthcheck":{"expectedCode":"200","urls":"http://localhost:8080/application/beans"}},"id":null},"moduleStates":{"httpHealthcheck":[{"status":"BAD","label":"c Heathcheck","properties":{"statusCode":"401"}}]}}],"links":[{"entity":{"name":"","origin":{"name":"a","appId":"first","labels":[],"configuration":{"httpHealthcheck":{"expectedCode":"200","urls":"http://localhost:8080/application/info"}},"id":null},"endNode":{"name":"b","appId":"second","labels":[],"configuration":{"httpHealthcheck":{"urls":"http://localhost:8080/application/health","expectedCode":"200"}},"id":null},"configuration":{"errorHighlight":{"onError":"true","color":"red"}},"id":null},"moduleStates":{"httpHealthcheck":[]}},{"entity":{"name":"","origin":{"name":"a","appId":"first","labels":[],"configuration":{"httpHealthcheck":{"expectedCode":"200","urls":"http://localhost:8080/application/info"}},"id":null},"endNode":{"name":"c","appId":"third","labels":[],"configuration":{"httpHealthcheck":{"expectedCode":"200","urls":"http://localhost:8080/application/beans"}},"id":null},"configuration":{"errorHighlight":{"onError":"true","color":"red"}},"id":null},"moduleStates":{"httpHealthcheck":[]}},{"entity":{"name":"","origin":{"name":"b","appId":"second","labels":[],"configuration":{"httpHealthcheck":{"urls":"http://localhost:8080/application/health","expectedCode":"200"}},"id":null},"endNode":{"name":"c","appId":"third","labels":[],"configuration":{"httpHealthcheck":{"expectedCode":"200","urls":"http://localhost:8080/application/beans"}},"id":null},"configuration":{},"id":null},"moduleStates":{"httpHealthcheck":[]}}]}
const w = window.innerWidth;
const h = window.innerHeight;

const svg = d3.select('body')
    .append('svg')
    .attr('width', w)
    .attr('height', h);

var simulation = d3.forceSimulation()
    .force("link", d3.forceLink().id(function(d) { return d["entity"]["appId"]; }))
    .force("charge", d3.forceManyBody().strength(-100))
    .force("center", d3.forceCenter(w / 2, h / 2));


d3.json("state", function(error, graph) {
    if (error) throw error;

    console.log(graph)
    var nodes = graph["applications"]
    var links = graph["links"]

    links.forEach(function(d){
        d.source = d["entity"]["origin"]["appId"];
        d.target = d["entity"]["endNode"]["appId"];
    });

    var link = svg.append("g")
        .style("stroke", "#aaa")
        .selectAll("line")
        .data(links)
        .enter().append("line");

    console.log(nodes)

    var node = svg.append("g")
        .attr("class", "nodes")
        .selectAll("circle")
        .data(nodes)
        .enter().append("circle")
        .attr("r", 6)
        .style("fill", function (d) { if (d["moduleStates"]["httpHealthcheck"][0]["status"] === "GOOD") return "#008000"; else return "#FF0000"; })

    .call(d3.drag()
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended));

    var label = svg.append("g")
        .attr("class", "labels")
        .selectAll("text")
        .data(nodes)
        .enter().append("text")
        .attr("class", "label")
        .text(function(d) { return d["entity"]["appName"]; });

    simulation.nodes(nodes)
        .on("tick", ticked);

    simulation.force("link")
        .links(links);

    function ticked() {
        link
            .attr("x1", function(d) { return d.source.x; })
            .attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; })
            .attr("y2", function(d) { return d.target.y; });

        node
            .attr("r", 20)
            .style("stroke", "#969696")
            .style("stroke-width", "1px")
            .attr("cx", function (d) { return d.x+6; })
            .attr("cy", function(d) { return d.y-6; });

        label
            .attr("x", function(d) { return d.x - (d["entity"]["appName"].length * 2.5); })
            .attr("y", function (d) { return d.y - 30; })
            .style("font-size", "12px").style("fill", "#4393c3");
    }
});
function dragstarted() {
    if (!d3.event.active) simulation.alphaTarget(0.3).restart();
    d3.event.subject.fx = d3.event.subject.x;
    d3.event.subject.fy = d3.event.subject.y;
}

function dragged() {
    d3.event.subject.fx = d3.event.x;
    d3.event.subject.fy = d3.event.y;
}

function dragended() {
    if (!d3.event.active) simulation.alphaTarget(0);
    d3.event.subject.fx = null;
    d3.event.subject.fy = null;
}

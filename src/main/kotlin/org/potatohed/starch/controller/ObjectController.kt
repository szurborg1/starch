package org.potatohed.starch.controller

import org.potatohed.starch.modules.ModuleRegistry
import org.potatohed.starch.modules.ModuleState
import org.potatohed.starch.modules.ModuleState.STATUS
import org.potatohed.starch.objects.ApplicationEntity
import org.potatohed.starch.objects.Applications
import org.potatohed.starch.objects.EntityState
import org.potatohed.starch.objects.LinkEntity
import org.potatohed.starch.objects.Links
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ObjectController(
    val applications: Applications,
    val links: Links,
    val registry: ModuleRegistry,
    val applicationEntities: List<ApplicationEntity>,
    val linkEntitys: List<LinkEntity>) {

    @GetMapping("/applications")
    fun getNodes() = applications.applications

    @GetMapping("/links")
    fun getRelationships() = links.links

    @GetMapping("/state")
    fun getModulesState() = State(
        applicationEntities.map { app -> EntityState(app, registry.modules.values().map { Pair(it.moduleId, it.state(app)) }.toMap()) },
        linkEntitys.map { link -> EntityState(link, registry.modules.values().map { Pair(it.moduleId, it.state(link)) }.toMap()) })

    @GetMapping("/graph")
    fun getGraph() = Graph(children = applicationEntities.map { app -> nodeFrom(app) })

    fun nodeFrom(applicationEntity: ApplicationEntity): Node {
        val states = registry.modules.values().map { it.state(applicationEntity) }.flatMap { it }
        return Node(name = applicationEntity.name,
            children = states.map { it.toNode() },
            color = when (states.worstStatus()) {
                STATUS.GOOD -> "#008000"
                STATUS.BAD -> "#FF0000"
                else -> "#FFFF00"
            })
    }

}

private fun List<ModuleState>.worstStatus(): STATUS {
    return this.map { it.status }.reduce { acc, i -> if (acc.severity < i.severity) i else acc }
}

data class State(
    val applications: List<EntityState<ApplicationEntity>>,
    val links: List<EntityState<LinkEntity>>
)

data class Graph(
    val name: String = "root",
    val children: List<Node>
)

data class Node(
    val name: String,
    val color: String,
    val children: List<Node>

)

package org.potatohed.starch.objects

import org.potatohed.starch.modules.ModuleState

interface Entity {
    var name : String
    var configuration: Map<String, Map<String, Any>>
}

interface ApplicationEntity : Entity {
    var appId: String
    var labels: List<String>
}

interface LinkEntity : Entity {
    var origin: ApplicationEntity
    var endNode: ApplicationEntity
}

data class EntityState<out T : Entity>(
        val entity: T,
        val moduleStates: Map<String, List<ModuleState>>)

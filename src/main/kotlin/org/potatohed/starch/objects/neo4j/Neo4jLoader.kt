package org.potatohed.starch.objects.neo4j

import org.neo4j.ogm.session.Session
import org.potatohed.starch.objects.ApplicationEntity
import org.potatohed.starch.objects.LinkEntity
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.data.neo4j.transaction.Neo4jTransactionManager
import org.springframework.stereotype.Component
import org.springframework.transaction.interceptor.DefaultTransactionAttribute

//@Component
class Neo4jLoader(val applicationNodes: List<ApplicationEntity>,
                  val relationships: List<LinkEntity>,
                  val session: Session,
                  val transactionManager: Neo4jTransactionManager) : ApplicationRunner {

    override fun run(args: ApplicationArguments?) {
        val transaction = transactionManager.getTransaction(DefaultTransactionAttribute())
        applicationNodes.forEach(session::save)
        relationships.forEach(session::save)
        transaction.flush()
        transactionManager.commit(transaction)
    }
}
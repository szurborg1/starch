package org.potatohed.starch.objects

interface LinkEntityMapper {
    fun convert(link: Link, applicationEntities : List<ApplicationEntity>) : List<LinkEntity>
}

interface ApplicationEntityMapper {

    fun convert(application : Application) : ApplicationEntity

}
package org.potatohed.starch.modules.builtin

import org.potatohed.starch.modules.InvocationPredicate

object EVERY_SECOND : InvocationPredicate {
    override fun frequency(): InvocationPredicate.Frequency { return InvocationPredicate.Frequency.SECOND }
    override fun shouldInvoke(): Boolean { return true }
}
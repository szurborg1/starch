package org.potatohed.starch.util

import org.apache.http.conn.ssl.SSLConnectionSocketFactory
import org.apache.http.impl.client.HttpClients
import org.springframework.http.client.ClientHttpRequestFactory
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import java.security.KeyManagementException
import java.security.KeyStoreException
import java.security.NoSuchAlgorithmException
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext

val noSSLValidationRequestFactory: ClientHttpRequestFactory
    get() {
        val csf = sslConnectionSocketFactory

        val httpClient = HttpClients.custom()
            .setSSLSocketFactory(csf)
            .build()
        val requestFactory = HttpComponentsClientHttpRequestFactory()
        requestFactory.httpClient = httpClient
        return requestFactory
    }

val sslConnectionSocketFactory: SSLConnectionSocketFactory
    get() {
        val acceptingTrustStrategy = { _: Array<X509Certificate>, _: String -> true }
        var sslContext: SSLContext? = null
        try {
            sslContext = org.apache.http.ssl.SSLContexts.custom()
                .loadTrustMaterial(null, acceptingTrustStrategy)
                .build()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: KeyManagementException) {
            e.printStackTrace()
        } catch (e: KeyStoreException) {
            e.printStackTrace()
        }

        return SSLConnectionSocketFactory(sslContext)
    }

